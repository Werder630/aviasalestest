package com.werder.aviasalestest.utils

import android.os.CountDownTimer

object CancelledTimer : CountDownTimer(300, 300) {
    private var action: (() -> Unit)? = null

    fun startWithResult(block: () -> Unit) {
        cancel()
        action = block
        start()
    }

    override fun onFinish() {
        action?.invoke()
    }

    override fun onTick(millisUntilFinished: Long) {}
}