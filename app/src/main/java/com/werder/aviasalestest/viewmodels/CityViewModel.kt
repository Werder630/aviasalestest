package com.werder.aviasalestest.viewmodels

import ApiFactory
import com.werder.aviasalestest.api.CityDataModel
import com.werder.aviasalestest.api.CityRepository

class CityViewModel : ViewModel() {

    private val parentJob = Job()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)

    private val repository = CityRepository(ApiFactory.cityApi)

    val cityLiveData = MutableLiveData<List<CityDataModel>>()

    fun loadCityByName(name: String){
        scope.launch {
            val items = repository.getCitiesByName(name)
            cityLiveData.postValue(items)
        }
    }

    fun cancelAllRequests() = coroutineContext.cancel()
}