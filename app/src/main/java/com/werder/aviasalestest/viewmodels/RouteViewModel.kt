package com.werder.aviasalestest.viewmodels

import com.werder.aviasalestest.api.CityDataModel

typealias Route = Pair<CityDataModel?, CityDataModel?>

typealias RouteCity = Int

const val ROUTE_CITY_FROM = 1
const val ROUTE_CITY_TO = 2

class RouteViewModel : ViewModel() {
    private var route = Route(null, null)
    var routeObservable = MutableLiveData<Route>()

    var fromCity: CityDataModel? = null
        set(value) {
            field = value
            route = route.copy(first = value)
            routeObservable.value = route
        }

    var toCity: CityDataModel? = null
        set(value) {
            field = value
            route = route.copy(second = value)
            routeObservable.value = route
        }
}