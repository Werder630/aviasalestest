package com.werder.aviasalestestproject.ui

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Paint.ANTI_ALIAS_FLAG
import com.werder.aviasalestest.R
import java.util.*

object MarkerBitmap {

    private val paint = Paint(ANTI_ALIAS_FLAG).apply {
        textSize = 50f
        color = Color.WHITE
        textAlign = Paint.Align.CENTER
    }

    fun getBitmap(context: Context, value: String): Bitmap {
        val threeLetters = value.substring(0, 3).toUpperCase(Locale.getDefault())
        val baseline = -paint.ascent()
        val width = (paint.measureText(threeLetters) + 120f).toInt()
        val height = (baseline + paint.descent() + 20f).toInt()
        val image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(image)

        AppCompatResources.getDrawable(context, R.drawable.marker)?.apply {
            setBounds(0, 0, image.width, image.height)
            draw(canvas)
        }

        canvas.drawText(threeLetters, width.toFloat() / 2, baseline + 10f, paint)
        return image
    }
}