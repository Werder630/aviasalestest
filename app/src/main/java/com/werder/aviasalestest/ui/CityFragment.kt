package com.werder.aviasalestest.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.werder.aviasalestest.R
import com.werder.aviasalestest.api.CityDataModel
import com.werder.aviasalestest.utils.CancelledTimer
import com.werder.aviasalestest.utils.SimpleTextWatcher
import com.werder.aviasalestest.viewmodels.CityViewModel
import com.werder.aviasalestest.viewmodels.ROUTE_CITY_FROM
import com.werder.aviasalestest.viewmodels.ROUTE_CITY_TO
import com.werder.aviasalestest.viewmodels.RouteViewModel
import kotlinx.android.synthetic.main.fragment_city.content_container as contentContainer
import kotlinx.android.synthetic.main.fragment_city.edit_text as editText

class CityFragment : Fragment() {

    private lateinit var cityViewModel: CityViewModel
    private lateinit var routeViewModel: RouteViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_city, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cityViewModel = ViewModelProviders.of(requireActivity()).get(CityViewModel::class.java)
        routeViewModel = ViewModelProviders.of(requireActivity()).get(RouteViewModel::class.java)

        val routeId = requireNotNull(arguments?.get("ROUTE_CITY_ID")) {
            "Fragment ${this.javaClass.canonicalName} must be launch with argument of routeId"
        }

        val listener: (CityDataModel) -> Unit = {
            when (routeId) {
                ROUTE_CITY_FROM -> routeViewModel.fromCity = it
                ROUTE_CITY_TO -> routeViewModel.toCity = it
            }
            findNavController().popBackStack()
        }

        val adapter = CityAdapter(listener).apply {
            contentContainer.adapter = this
            contentContainer.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        }

        cityViewModel.cityLiveData.observe(this, Observer {
            if (it.isEmpty()) {
                flipper.showEmpty()
            } else {
                flipper.showContent()
                adapter.submitList(it)
            }
        })

        editText.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                super.onTextChanged(s, start, before, count)
                CancelledTimer.startWithResult {
                    cityViewModel.loadCityByName(s.toString())
                }
            }
        })
    }
}