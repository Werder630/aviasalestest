package com.werder.aviasalestest.ui

import android.animation.ValueAnimator
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import com.werder.aviasalestest.R
import com.werder.aviasalestest.viewmodels.RouteViewModel
import com.werder.aviasalestestproject.ui.MarkerBitmap

private const val MAP_PADDING = 200

class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var routeViewModel: RouteViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        routeViewModel = ViewModelProviders.of(requireActivity()).get(RouteViewModel::class.java)
        val map = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        map.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap) {
        val polyline = PolylineOptions().pattern(listOf(Dot(), Gap(10f)))
        val bounds = LatLngBounds.Builder()
        var plane: Marker
        var destination: LatLng

        routeViewModel.routeObservable.observe(this, Observer {
            with(requireNotNull(it.first)) {
                val coordinates = LatLng(location.latitude, location.longitude)
                map.addMarker(MarkerOptions()
                        .position(coordinates)
                        .icon(BitmapDescriptorFactory
                                .fromBitmap(MarkerBitmap.getBitmap(requireContext(), city))))

                plane = map.addMarker(MarkerOptions()
                        .position(coordinates)
                        .anchor(0.5.toFloat(), 0.5.toFloat())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_plane)))

                bounds.include(coordinates)
                polyline.add(coordinates)
            }

            with(requireNotNull(it.second)) {
                destination = LatLng(location.latitude, location.longitude)
                map.addMarker(MarkerOptions()
                        .position(destination)
                        .icon(BitmapDescriptorFactory.fromBitmap(MarkerBitmap.getBitmap(requireContext(), city))))
                bounds.include(destination)
                polyline.add(destination)
            }

            map.addPolyline(polyline)
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), MAP_PADDING))

            animatePlane(plane, destination)
        })
    }

    private fun animatePlane(plane: Marker, destination: LatLng) {
        val startPosition = plane.position
        plane.rotation = destination.toLocation().bearingTo(startPosition.toLocation())
        ValueAnimator.ofFloat(0f, 1f).apply {
            val distance = Math.sqrt(
                    Math.pow(startPosition.latitude - destination.latitude, 2.toDouble()) +
                            Math.pow(startPosition.longitude - destination.longitude, 2.toDouble())
            )

            duration = (distance * 1000).toLong()
            interpolator = DecelerateInterpolator()
            addUpdateListener {
                plane.position = interpolate(it.animatedFraction, startPosition, destination)
            }
            doOnEnd {
                start()
            }
        }.start()
    }
}

private fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
    val lat = (b.latitude - a.latitude) * fraction + a.latitude
    var lngDelta = b.longitude - a.longitude
    if (Math.abs(lngDelta) > 180) {
        lngDelta -= Math.signum(lngDelta) * 360
    }
    val lng = lngDelta * fraction + a.longitude
    return LatLng(lat, lng)
}

private fun LatLng.toLocation(): Location {
    return Location("").apply {
        latitude = this@toLocation.latitude
        longitude = this@toLocation.longitude
    }
}
