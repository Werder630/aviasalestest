package com.werder.aviasalestest.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.werder.aviasalestest.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
