package com.werder.aviasalestest.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.werder.aviasalestest.R
import com.werder.aviasalestest.viewmodels.ROUTE_CITY_FROM
import com.werder.aviasalestest.viewmodels.ROUTE_CITY_TO
import com.werder.aviasalestest.viewmodels.RouteViewModel

class DataFragment : Fragment() {

    private lateinit var routeViewModel: RouteViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_data, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

        routeViewModel = ViewModelProviders.of(requireActivity()).get(RouteViewModel::class.java)
        routeViewModel.routeObservable.observe(requireActivity(), Observer {
            card_city_from?.model = it.first
            card_city_to?.model = it.second
        })

        card_city_from.setOnClickListener {
            val bundle = bundleOf("ROUTE_CITY_ID" to ROUTE_CITY_FROM)
            findNavController().navigate(R.id.action_data_to_city, bundle)
        }

        card_city_to.setOnClickListener {
            val bundle = bundleOf("ROUTE_CITY_ID" to ROUTE_CITY_TO)
            findNavController().navigate(R.id.action_data_to_city, bundle)
        }

        go_to_map.setOnClickListener {
            findNavController().navigate(R.id.action_data_to_map)
        }
    }

    private fun initView() {
        card_city_from.destination = "From"
        card_city_to.destination = "To"
    }
}