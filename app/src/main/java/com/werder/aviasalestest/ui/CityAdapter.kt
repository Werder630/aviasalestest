package com.werder.aviasalestest.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListAdapter
import android.widget.TextView
import com.werder.aviasalestest.R
import com.werder.aviasalestest.api.CityDataModel

class CityAdapter(private val click: (CityDataModel) -> Unit)
    : ListAdapter<CityDataModel, CityAdapter.ViewHolder>(CityDiffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_city, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val city = getItem(position)
        holder.itemView.setOnClickListener { click(city) }
        holder.cityTextView.text = city.city
        holder.countryTextView.text = city.country
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cityTextView: TextView = view.findViewById(R.id.city)
        val countryTextView: TextView = view.findViewById(R.id.country)
    }
}

object CityDiffUtil : DiffUtil.ItemCallback<CityDataModel>() {
    override fun areItemsTheSame(oldItem: CityDataModel, newItem: CityDataModel) = oldItem == newItem
    override fun areContentsTheSame(oldItem: CityDataModel, newItem: CityDataModel) = oldItem == newItem
}