package com.werder.aviasalestest.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import com.werder.aviasalestest.R
import com.werder.aviasalestest.api.CityDataModel

class CityCardView(
    context: Context,
    attrs: AttributeSet
) : MaterialCardView(context, attrs) {

    private val destinationTextView: TextView
    private val valueTextView: TextView

    var model: CityDataModel? = null
        set(value) {
            field = value
            value?.let {
                valueTextView.text = value.city
            }
        }

    var destination: String? = null
        set(value) {
            field = value
            destinationTextView.text = value
        }

    init {
        val root = LayoutInflater.from(context).inflate(R.layout.card_city, this, true)
        radius = resources.getDimension(R.dimen.card_raduis)
        destinationTextView = root.findViewById(R.id.destination)
        valueTextView = root.findViewById(R.id.value)
    }
}