package com.werder.aviasalestest.api

import retrofit2.Response

open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>,
                                      errorMessage: String): T? {

        return when(val result = safeApiResult(call, errorMessage)) {
            is Result.Success -> result.data
            is Result.Failure -> null
        }
    }

    private suspend fun <T: Any> safeApiResult(call: suspend ()-> Response<T>, errorMessage: String) : Result<T>{
        val response = call()
        return if (response.isSuccessful)
            Result.Success(requireNotNull(response.body()))
        else
            Result.Failure(Exception(errorMessage))
    }
}