package com.werder.aviasalestest.api

class CityRepository(private val api: CityService) : BaseRepository() {

    suspend fun getCitiesByName(name: String) : List<CityDataModel> {
        val response = safeApiCall(
            call = { api.getCitiesAsync(name, "123").await() },
            errorMessage = "Some problem"
        )

        return requireNotNull(response?.cities)
    }
}