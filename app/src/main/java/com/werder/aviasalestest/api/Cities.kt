package com.werder.aviasalestest.api

data class Cities(
    @SerializedName("cities")
    val cities: List<CityDataModel>
)

data class CityDataModel(
    @SerializedName("country")
    val country: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("location")
    val location: Location
)

data class Location(
    @SerializedName("lat")
    val latitude: Double,
    @SerializedName("lon")
    val longitude: Double
)