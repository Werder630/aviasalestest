package com.werder.aviasalestest.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CityService {
    @GET("/autocomplete")
    fun getCitiesAsync(
        @Query("term") name: String,
        @Query("lang") lang: String = "en")
            : Deferred<Response<Cities>>
}